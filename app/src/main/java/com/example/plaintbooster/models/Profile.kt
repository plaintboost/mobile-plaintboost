package com.example.plaintbooster.models

data class Profile(
    val id: Int? = null,
    val name: String? = null,
    val username: String? = null,
    val email: String? = null,
    val nik: String? = null,
    val telp: String? = null,
    val role_user: String? = null
)