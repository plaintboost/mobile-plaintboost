package com.example.plaintbooster.models.authentication

import com.example.plaintbooster.models.Profile

data class Authentication(
    val token: String? = null,
    val profile: Profile? = Profile()
)