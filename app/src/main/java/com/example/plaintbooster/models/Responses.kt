package com.example.plaintbooster.models

data class Responses(
    val id: Int? = null,
    val id_pengaduan: Int? = null,
    val tanggapan: String? = null,
    val id_petugas: Int? = null,
    val created_at: String? = null,
    val updated_at: String? = null,
    val petugas: Profile? = Profile()
)