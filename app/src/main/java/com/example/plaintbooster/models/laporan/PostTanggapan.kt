package com.example.plaintbooster.models.laporan

import com.example.plaintbooster.models.Complaint
import com.example.plaintbooster.models.Profile

data class PostTanggapan(
    val response: String? = null,
    val officer: Profile? = Profile(),
    val complaint: Complaint? = Complaint()
)