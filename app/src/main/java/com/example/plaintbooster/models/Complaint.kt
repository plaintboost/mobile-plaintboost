package com.example.plaintbooster.models

data class Complaint(
    val id: Int? = null,
    val id_user: Int? = null,
    val judul_laporan: String? = null,
    val laporan: String? = null,
    val created_at: String? = null,
    val updated_at: String? = null,
    val foto: List<Foto>? = listOf(),
    val data: List<Complaint>? = listOf(),
    val verified: Int? = null,
    val current_page: Int? = null,
    val total: Int? = null,
    val user: Profile? = Profile()
)