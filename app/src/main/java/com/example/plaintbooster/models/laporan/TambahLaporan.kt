package com.example.plaintbooster.models.laporan

import com.example.plaintbooster.models.Complaint
import com.example.plaintbooster.models.Profile

data class TambahLaporan(
    val complainant: Profile? = Profile(),
    val complaint: Complaint? = Complaint()
)