package com.example.plaintbooster.models

data class Foto(
    val id: Int? = null,
    val id_pengaduan: Int? = null,
    val foto: String? = null,
    val created_at: String? = null,
    val updated_at: String? = null
)