package com.example.plaintbooster.models.error

data class Error(
    val errors: List<String>? = listOf()
)