package com.example.plaintbooster.models.laporan

import com.example.plaintbooster.models.Complaint
import com.example.plaintbooster.models.Responses

data class GetLaporan(
    val complaint: Complaint? = null,
    val total_complaint: Int? = null,
    val response: Responses? = null
)