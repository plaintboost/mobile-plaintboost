package com.example.plaintbooster.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.plaintbooster.models.laporan.GetLaporan
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse

class MainViewModel: ViewModel(), ViewModelCallback<GetLaporan> {

    private var laporanData: MutableLiveData<ApiResponse<GetLaporan>> = MutableLiveData()
    private var laporanErrorData: MutableLiveData<ApiErrorResponse> = MutableLiveData()

    fun getLaporan(token: String, actor: String, verified: Boolean, page: Int, search: String, dataManagerService: DataManagerService) {
        dataManagerService.getLaporan(token, actor, verified, page, search,this)
    }

    fun getLaporanData(): LiveData<ApiResponse<GetLaporan>> = laporanData

    fun getLaporanErrorData(): LiveData<ApiErrorResponse> = laporanErrorData

    override fun resultCallback(result: ApiResponse<GetLaporan>?) {
       laporanData.postValue(result)
    }

    override fun errorCallback(result: ApiErrorResponse?) {
        laporanErrorData.postValue(result)
    }

}