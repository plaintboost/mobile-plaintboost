package com.example.plaintbooster.network

import android.util.Log
import com.example.plaintbooster.models.authentication.Authentication
import com.example.plaintbooster.models.laporan.GetLaporan
import com.example.plaintbooster.models.laporan.PostTanggapan
import com.example.plaintbooster.models.laporan.TambahLaporan
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse
import com.google.gson.Gson
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataManagerService {

    private val apiService = Retrofit().dataManager

    fun login(email: String, password: String, callback: ViewModelCallback<Authentication>) {
        apiService.login(email, password).enqueue(object: Callback<ApiResponse<Authentication>> {
            override fun onFailure(call: Call<ApiResponse<Authentication>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<Authentication>>,
                response: Response<ApiResponse<Authentication>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

    fun register(
        name: String,
        username: String,
        email: String,
        password: String,
        nik: String,
        telp: String,
        callback: ViewModelCallback<Authentication>
    ) {
        apiService.register(name, username, email, password, nik, telp).enqueue(object: Callback<ApiResponse<Authentication>> {
            override fun onFailure(call: Call<ApiResponse<Authentication>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<Authentication>>,
                response: Response<ApiResponse<Authentication>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

    fun tambahLaporanWithFoto(
        token: String,
        judulLaporan: RequestBody,
        laporan: RequestBody,
        imageRequestBody: List<MultipartBody.Part>,
        callback: ViewModelCallback<TambahLaporan>
    ) {
        apiService.tambahLaporanWithFoto(token, judulLaporan, laporan, imageRequestBody).enqueue(object: Callback<ApiResponse<TambahLaporan>> {
            override fun onFailure(call: Call<ApiResponse<TambahLaporan>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<TambahLaporan>>,
                response: Response<ApiResponse<TambahLaporan>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    Log.d(DataManagerService::class.java.simpleName, "ERROR DATA = ${response.errorBody()?.string()}")
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

    fun tambahLaporan(
        token: String,
        judulLaporan: String,
        laporan: String,
        callback: ViewModelCallback<TambahLaporan>
    ) {
        apiService.tambahLaporan(token, judulLaporan, laporan).enqueue(object : Callback<ApiResponse<TambahLaporan>> {
            override fun onFailure(call: Call<ApiResponse<TambahLaporan>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<TambahLaporan>>,
                response: Response<ApiResponse<TambahLaporan>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

    fun getLaporan(
        token: String,
        actor: String,
        verified: Boolean,
        page: Int,
        search: String,
        callback: ViewModelCallback<GetLaporan>
    ) {
        apiService.getLaporan(token, actor, verified, page, search).enqueue(object: Callback<ApiResponse<GetLaporan>> {
            override fun onFailure(call: Call<ApiResponse<GetLaporan>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<GetLaporan>>,
                response: Response<ApiResponse<GetLaporan>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

    fun getDetailLaporan(token: String, id: Int, callback: ViewModelCallback<GetLaporan>) {
        apiService.getDetailLaporan(token, id).enqueue(object: Callback<ApiResponse<GetLaporan>> {
            override fun onFailure(call: Call<ApiResponse<GetLaporan>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<GetLaporan>>,
                response: Response<ApiResponse<GetLaporan>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

    fun postTanggapan(token: String, id: Int, tanggapan: String, callback: ViewModelCallback<PostTanggapan>) {
        apiService.postTanggapan(token, id, tanggapan).enqueue(object: Callback<ApiResponse<PostTanggapan>> {
            override fun onFailure(call: Call<ApiResponse<PostTanggapan>>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(
                call: Call<ApiResponse<PostTanggapan>>,
                response: Response<ApiResponse<PostTanggapan>>
            ) {
                if(response.isSuccessful) {
                    callback.resultCallback(response.body())
                } else {
                    val convertData =  Gson().fromJson(response.errorBody()?.string(), ApiErrorResponse::class.java)
                    callback.errorCallback(convertData)
                }
            }
        })
    }

}