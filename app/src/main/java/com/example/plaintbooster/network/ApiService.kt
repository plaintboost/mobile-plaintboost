package com.example.plaintbooster.network

import com.example.plaintbooster.models.authentication.Authentication
import com.example.plaintbooster.models.laporan.GetLaporan
import com.example.plaintbooster.models.laporan.PostTanggapan
import com.example.plaintbooster.models.laporan.TambahLaporan
import com.example.plaintbooster.network.response.ApiResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @FormUrlEncoded
    @POST("/api/auth/login")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<ApiResponse<Authentication>>

    @FormUrlEncoded
    @POST("/api/auth/register")
    fun register(
        @Field("name") name: String,
        @Field("username") username: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("nik") nik: String,
        @Field("telp") telp: String
    ): Call<ApiResponse<Authentication>>

    /**
     * @Authorization digunakan untuk validasi token
     * */
    @Multipart
    @POST("/api/masyarakat/pengaduan")
    fun tambahLaporanWithFoto(
        @Header("Authorization") token: String,
        @Part("judul_laporan") judul: RequestBody,
        @Part("laporan") laporan: RequestBody,
        @Part foto: List<MultipartBody.Part>
    ): Call<ApiResponse<TambahLaporan>>

    @FormUrlEncoded
    @POST("/api/masyarakat/pengaduan")
    fun tambahLaporan(
        @Header("Authorization") token: String,
        @Field("judul_laporan") judul: String,
        @Field("laporan") laporan: String
    ): Call<ApiResponse<TambahLaporan>>

    @GET("/api/{actor}/pengaduan")
    fun getLaporan(
        @Header("Authorization") token: String,
        @Path("actor") actor: String,
        @Query("verified") verified: Boolean,
        @Query("page") page: Int,
        @Query("q") search: String
    ): Call<ApiResponse<GetLaporan>>

    @GET("/api/pengaduan/{id}")
    fun getDetailLaporan(
        @Header("Authorization") token: String,
        @Path("id") id: Int
    ): Call<ApiResponse<GetLaporan>>

    @FormUrlEncoded
    @POST("/api/petugas/pengaduan/{id}/tanggapan")
    fun postTanggapan(
        @Header("Authorization") token: String,
        @Path("id") id: Int,
        @Field("tanggapan") tanggapan: String
    ): Call<ApiResponse<PostTanggapan>>

}