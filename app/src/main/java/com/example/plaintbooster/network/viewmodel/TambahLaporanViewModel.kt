package com.example.plaintbooster.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.plaintbooster.models.laporan.TambahLaporan
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

class TambahLaporanViewModel: ViewModel(), ViewModelCallback<TambahLaporan> {

    private var tambahLaporanData = MutableLiveData<ApiResponse<TambahLaporan>>()
    private var tambahLaporanErrorData = MutableLiveData<ApiErrorResponse>()

    fun getTambahLaporanData(): LiveData<ApiResponse<TambahLaporan>> = tambahLaporanData

    fun getTambahLaporanErrorData(): LiveData<ApiErrorResponse> = tambahLaporanErrorData

    fun postTambahLaporan(
        token: String,
        judulLaporan: RequestBody,
        laporan: RequestBody,
        imageLaporan: List<MultipartBody.Part>,
        dataManagerService: DataManagerService
    ) {
        dataManagerService.tambahLaporanWithFoto(token, judulLaporan, laporan, imageLaporan, this)
    }

    fun postTambahLaporan(
        token: String,
        judulLaporan: String,
        laporan: String,
        dataManagerService: DataManagerService
    ) {
        dataManagerService.tambahLaporan(token, judulLaporan, laporan, this)
    }

    override fun resultCallback(result: ApiResponse<TambahLaporan>?) {
        tambahLaporanData.postValue(result)
    }

    override fun errorCallback(result: ApiErrorResponse?) {
        tambahLaporanErrorData.postValue(result)
    }

}