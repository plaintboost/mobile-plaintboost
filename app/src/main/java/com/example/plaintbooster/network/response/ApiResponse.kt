package com.example.plaintbooster.network.response

data class ApiResponse<Model>(
    val success: Boolean? = null,
    val message: Message? = Message(),
    val data: Model? = null
)