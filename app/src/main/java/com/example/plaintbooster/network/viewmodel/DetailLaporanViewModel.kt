package com.example.plaintbooster.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.plaintbooster.models.laporan.GetLaporan
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse

class DetailLaporanViewModel : ViewModel(), ViewModelCallback<GetLaporan> {

    private var laporanData: MutableLiveData<ApiResponse<GetLaporan>> = MutableLiveData()
    private var laporanErrorData: MutableLiveData<ApiErrorResponse> = MutableLiveData()

    fun getDetailLaporan(token: String, id: Int, dataManagerService: DataManagerService) {
        dataManagerService.getDetailLaporan(token, id, this)
    }

    fun getLaporanData(): LiveData<ApiResponse<GetLaporan>> = laporanData

    fun getLaporanErrorData(): LiveData<ApiErrorResponse> = laporanErrorData

    override fun resultCallback(result: ApiResponse<GetLaporan>?) {
        laporanData.postValue(result)
    }

    override fun errorCallback(result: ApiErrorResponse?) {
        laporanErrorData.postValue(result)
    }

}