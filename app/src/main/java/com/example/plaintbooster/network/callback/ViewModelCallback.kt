package com.example.plaintbooster.network.callback

import com.example.plaintbooster.models.authentication.Authentication
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse

interface ViewModelCallback<Model> {
    fun resultCallback(result: ApiResponse<Model>?)
    fun errorCallback(result: ApiErrorResponse?)
}