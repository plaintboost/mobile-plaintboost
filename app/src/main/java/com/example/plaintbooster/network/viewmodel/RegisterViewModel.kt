package com.example.plaintbooster.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.plaintbooster.models.authentication.Authentication
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse

class RegisterViewModel: ViewModel(), ViewModelCallback<Authentication> {

    private var registerData = MutableLiveData<ApiResponse<Authentication>>()
    private var registerErrorData = MutableLiveData<ApiErrorResponse>()

    fun register(name: String, username: String, email: String, password: String, nik: String, telp: String, dataManagerService: DataManagerService) {
        dataManagerService.register(name, username, email, password, nik, telp, this)
    }

    fun getLoginData(): LiveData<ApiResponse<Authentication>> = registerData

    fun getErrorLoginData(): LiveData<ApiErrorResponse> = registerErrorData

    override fun resultCallback(result: ApiResponse<Authentication>?) {
        registerData.postValue(result)
    }

    override fun errorCallback(result: ApiErrorResponse?) {
        registerErrorData.postValue(result)
    }


}