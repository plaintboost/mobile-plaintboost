package com.example.plaintbooster.network.response

import com.example.plaintbooster.models.error.Error

data class ApiErrorResponse(
    val success: Boolean? = null,
    val message: Message? = Message(),
    val data: Error? = Error()
)