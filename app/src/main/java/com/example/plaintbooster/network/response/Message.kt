package com.example.plaintbooster.network.response

data class Message(
    val message: String? = null
)