package com.example.plaintbooster.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.plaintbooster.models.authentication.Authentication
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse

class LoginViewModel: ViewModel(), ViewModelCallback<Authentication> {

    private var loginData = MutableLiveData<ApiResponse<Authentication>>()
    private var loginErrorData = MutableLiveData<ApiErrorResponse>()

    fun login(email: String, password: String, dataManagerService: DataManagerService) {
        dataManagerService.login(email, password, this)
    }

    fun getLoginData(): LiveData<ApiResponse<Authentication>> = loginData

    fun getErrorLoginData(): LiveData<ApiErrorResponse> = loginErrorData

    override fun resultCallback(result: ApiResponse<Authentication>?) {
        loginData.postValue(result)
    }

    override fun errorCallback(result: ApiErrorResponse?) {
        loginErrorData.postValue(result)
    }

}