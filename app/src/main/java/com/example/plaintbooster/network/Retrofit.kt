package com.example.plaintbooster.network

import com.example.plaintbooster.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Retrofit {

    private val BASE_URL = BuildConfig.BASE_URL
    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val dataManager = retrofit.create(ApiService::class.java)

}