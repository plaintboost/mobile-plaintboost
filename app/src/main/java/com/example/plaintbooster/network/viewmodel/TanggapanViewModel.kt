package com.example.plaintbooster.network.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.plaintbooster.models.laporan.PostTanggapan
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.callback.ViewModelCallback
import com.example.plaintbooster.network.response.ApiErrorResponse
import com.example.plaintbooster.network.response.ApiResponse

class TanggapanViewModel: ViewModel(), ViewModelCallback<PostTanggapan> {

    private var tanggapanData: MutableLiveData<ApiResponse<PostTanggapan>> = MutableLiveData()
    private var tanggapanErrorData: MutableLiveData<ApiErrorResponse> = MutableLiveData()

    fun postTanggapan(token: String, id: Int, tanggapan: String, dataManagerService: DataManagerService) {
        dataManagerService.postTanggapan(token, id, tanggapan, this)
    }

    fun getTanggapanData(): LiveData<ApiResponse<PostTanggapan>> = tanggapanData

    fun getTanggapanErrorData(): LiveData<ApiErrorResponse> = tanggapanErrorData

    override fun resultCallback(result: ApiResponse<PostTanggapan>?) {
        tanggapanData.postValue(result)
    }

    override fun errorCallback(result: ApiErrorResponse?) {
        tanggapanErrorData.postValue(result)
    }

}