package com.example.plaintbooster.utils

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.FragmentActivity
import com.example.plaintbooster.R
import com.example.plaintbooster.databases.ProfileModel
import com.example.plaintbooster.databases.database
import com.example.plaintbooster.models.Profile
import com.example.plaintbooster.network.DataManagerService
import com.google.android.material.snackbar.Snackbar
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    fun showLoading(progressBar: ProgressBar) {
        progressBar.visibility = View.VISIBLE
    }

    fun hideLoading(progressBar: ProgressBar) {
        progressBar.visibility = View.GONE
    }

    fun showErrorSnackBar(message: String, view: View) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show()
    }

    fun <Model> getErrorList(collection: List<Model>): String {
        var errorMessage = ""
        collection.forEachIndexed { index, element ->
            errorMessage += element
            if(index != collection.lastIndex) {
                errorMessage += "\n"
            }
        }
        return errorMessage
    }

    fun createAlertDialog(
        fragmentActivity: FragmentActivity,
        title: Int,
        message: Int,
        yesTitle: Int,
        noTitle: Int? = null,
        callback: ValidationDialogCallback? = null,
        positiveButtonListener: (dialog: DialogInterface, which: Int) -> Unit
    ): AlertDialog.Builder {
        val builder = AlertDialog.Builder(fragmentActivity)
        builder.apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(yesTitle) { dialog, which ->
                positiveButtonListener(dialog, which)
            }
            if(callback != null && noTitle != null) {
                setNegativeButton(noTitle) { dialog, which ->
                    callback.setNegativeButton(dialog, which)
                }
            }
        }
        builder.create()
        return builder
    }

    fun getRealPathFromURI(context: Context, contentURI: Uri): String? {
        val result: String?
        val cursor = context.contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun getToken(context: Context): String {
        var token = ""
        try {
            context.database.use {
                val result = select(ProfileModel.TABLE_PROFILE, ProfileModel.PROFILE_TOKEN)
                token = result.parseSingle(classParser<String>())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return token
    }

    const val DEFAULT_FORMAT = "dd MMM yyyy"
    @SuppressLint("SimpleDateFormat")
    fun getFormatTime(format: String, timestamp: String): String {
        var result = ""
        try {
            val sdf = SimpleDateFormat(format)
            val inputFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val tmpstmp = inputFormatter.parse(timestamp)
            result = sdf.format(tmpstmp)
        } catch(e: Exception) {
            e.printStackTrace()
        }
        return result
    }

    fun getUserRole(context: Context): String {
        var role = ""
        try {
            context.database.use {
                val result = select(ProfileModel.TABLE_PROFILE, ProfileModel.PROFILE_ROLE)
                role = result.parseSingle(classParser())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return role
    }

    fun getUserData(context: Context): ProfileModel {
        val profile: MutableList<ProfileModel> = mutableListOf()
        try {
            context.database.use {
                val result = select(ProfileModel.TABLE_PROFILE)
                val resultData = result.parseList(classParser<ProfileModel>())
                profile.addAll(resultData)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.d("UTILS", "PROFILE DATA = $profile")
        return profile[0]
    }

}