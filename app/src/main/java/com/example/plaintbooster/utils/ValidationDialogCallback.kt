package com.example.plaintbooster.utils

import android.content.DialogInterface

interface ValidationDialogCallback {
    fun setNegativeButton(dialog: DialogInterface, which: Int)
}