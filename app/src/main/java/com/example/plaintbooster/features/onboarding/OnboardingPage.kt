package com.example.plaintbooster.features.onboarding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.plaintbooster.R
import com.example.plaintbooster.features.auth.AuthActivity
import com.example.plaintbooster.features.onboarding.highlights.DataRapihFragment
import com.example.plaintbooster.features.onboarding.highlights.DetailPengaduanFragment
import com.example.plaintbooster.features.onboarding.highlights.PengaduanMasyarakatFragment
import com.example.plaintbooster.utils.PreferenceSetting
import com.orhanobut.hawk.Hawk
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator
import org.jetbrains.anko.find

class OnboardingPage : AppCompatActivity(), ViewPager.OnPageChangeListener {

    private lateinit var preferenceSetting: PreferenceSetting
    private lateinit var viewPager: ViewPager
    private lateinit var dotsIndicator: DotsIndicator
    private lateinit var buttonMulai: Button

    override fun onResume() {
        super.onResume()
        preferenceSetting = PreferenceSetting(applicationContext)
        preferenceSetting.setPreferenceFirstTimeInstall(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)
        initComponents()
        Hawk.init(applicationContext).build()
    }

    private fun initComponents() {
        viewPager = find(R.id.highlight_view_pager)
        dotsIndicator = find(R.id.highlight_dots_indicator)
        buttonMulai = find(R.id.welcome_page_btn_mulai)

        // set view pager adapter
        val viewPagerAdapter = HighlightSliderAdapter(supportFragmentManager)
        viewPager.adapter = viewPagerAdapter
        dotsIndicator.setViewPager(viewPager)
        viewPager.addOnPageChangeListener(this)

        // set button clicked
        buttonMulai.setOnClickListener {
            startActivity(Intent(this@OnboardingPage, AuthActivity::class.java))
            finish()
        }
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }


    private inner class HighlightSliderAdapter(fa: FragmentManager): FragmentStatePagerAdapter(fa) {
        override fun getItem(position: Int): Fragment {
            return when(position) {
                0 -> PengaduanMasyarakatFragment()
                1 -> DetailPengaduanFragment()
                2 -> DataRapihFragment()
                else -> DataRapihFragment()
            }
        }

        override fun getCount(): Int = 3
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        if(viewPager.currentItem == 2) {
            dotsIndicator.visibility = View.GONE
            buttonMulai.visibility = View.VISIBLE
        } else {
            dotsIndicator.visibility = View.VISIBLE
            buttonMulai.visibility = View.GONE
        }
    }
}
