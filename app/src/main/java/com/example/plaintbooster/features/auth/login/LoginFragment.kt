package com.example.plaintbooster.features.auth.login

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.plaintbooster.features.MainActivity

import com.example.plaintbooster.R
import com.example.plaintbooster.databases.ProfileModel
import com.example.plaintbooster.databases.database
import com.example.plaintbooster.models.Profile
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.viewmodel.LoginViewModel
import com.example.plaintbooster.utils.Utils
import com.example.plaintbooster.utils.Utils.getErrorList
import com.example.plaintbooster.utils.Utils.hideLoading
import com.example.plaintbooster.utils.Utils.showLoading
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.find
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.db.insert

class LoginFragment : Fragment() {

    private lateinit var emailInput: EditText
    private lateinit var passwordInput: EditText
    private lateinit var masuk: Button
    private lateinit var forgetPassword: TextView
    private lateinit var daftar: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var container: FrameLayout

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initComponents()
    }

    private fun initComponents() {
        emailInput = find(R.id.login_email_input)
        passwordInput = find(R.id.login_password_input)
        masuk = find(R.id.login_button_login)
        forgetPassword = find(R.id.login_forget_password)
        daftar = find(R.id.login_register_button)
        progressBar = find(R.id.login_progress_bar)
        container = find(R.id.login_container)

        loginViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(LoginViewModel::class.java)

        masuk.setOnClickListener {
            if(emailInput.text.toString().trim().isEmpty() || passwordInput.text.toString().trim().isEmpty()) {
                container.snackbar("Isi bagian email atau password")
            } else {
                loginViewModel.login(emailInput.text.toString(), passwordInput.text.toString(), DataManagerService())
                showLoading(progressBar)
            }
        }

        daftar.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_loginFragment_to_registerFragment)
        }

        observeLiveData()
    }

    private fun observeLiveData() {
        loginViewModel.getLoginData().observe(viewLifecycleOwner, Observer {
            if(it != null) {
                if(it.success!!) {
                    storeUserDataToDatabase(it.data?.profile, it.data?.token)
                } else {
                    hideLoading(progressBar)
                    container.snackbar("Something problem").show()
                    clearText()
                }
            }
        })

        loginViewModel.getErrorLoginData().observe(viewLifecycleOwner, Observer {
            if(it != null) {
                val errorMessage = getErrorList(it.data?.errors!!)
                hideLoading(progressBar)
                container.snackbar(errorMessage).show()
                clearText()
            }
        })
    }

    private fun storeUserDataToDatabase(profile: Profile?, token: String?) {
        try {
            requireContext().database.use {
                insert(ProfileModel.TABLE_PROFILE,
                ProfileModel.PROFILE_ID to profile?.id,
                ProfileModel.PROFILE_NIK to profile?.nik,
                ProfileModel.PROFILE_NAME to profile?.name,
                ProfileModel.PROFILE_USERNAME to profile?.username,
                ProfileModel.PROFILE_TELP to profile?.telp,
                ProfileModel.PROFILE_EMAIL to profile?.email,
                ProfileModel.PROFILE_ROLE to profile?.role_user,
                ProfileModel.PROFILE_TOKEN to token)
            }
            startActivity(intentFor<MainActivity>())
            activity?.finish()
        } catch (e: SQLiteConstraintException) {
            container.snackbar(e.localizedMessage).show()
        }
    }

    private fun clearText() {
        emailInput.setText("")
        passwordInput.setText("")
    }

}
