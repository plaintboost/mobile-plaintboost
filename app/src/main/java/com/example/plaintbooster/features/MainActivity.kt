package com.example.plaintbooster.features

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.plaintbooster.R
import com.example.plaintbooster.adapter.LaporanListAdapter
import com.example.plaintbooster.databases.ProfileModel
import com.example.plaintbooster.databases.database
import com.example.plaintbooster.features.auth.AuthActivity
import com.example.plaintbooster.features.laporan.detaillaporan.DetailLaporanActivity
import com.example.plaintbooster.features.laporan.tambahlaporan.TambahLaporanActivity
import com.example.plaintbooster.models.Complaint
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.viewmodel.MainViewModel
import com.example.plaintbooster.utils.EndlessRecyclerViewScrollListener
import com.example.plaintbooster.utils.Utils
import com.example.plaintbooster.utils.Utils.getToken
import com.example.plaintbooster.utils.Utils.getUserData
import com.example.plaintbooster.utils.ValidationDialogCallback
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import org.jetbrains.anko.intentFor

class MainActivity : AppCompatActivity(), View.OnClickListener, ValidationDialogCallback {

    private lateinit var btnLaporkan: LinearLayout
    private lateinit var btnLaporkanEmpty: Button
    private lateinit var btnLogout: LinearLayout
    private lateinit var container: SwipeRefreshLayout
    private lateinit var rvLaporan: RecyclerView
    private lateinit var noDataContainer: LinearLayout
    private lateinit var filterButton: ImageView
    private lateinit var petugasText: LinearLayout
    private lateinit var mainNoDataDesc: TextView
    private lateinit var accountName: TextView
    private lateinit var nikNumber: TextView
    private lateinit var edtSearch: EditText

    private var page = 1
    private var extraRole = ""
    private lateinit var profile: ProfileModel
    private var searchQuery = ""
    private var verified = false

    private var itemList: MutableList<Complaint> = mutableListOf()
    private lateinit var laporanListAdapter: LaporanListAdapter
    private lateinit var mainViewModel: MainViewModel
    private lateinit var scrollListener: EndlessRecyclerViewScrollListener
    private lateinit var linearLayoutManager: LinearLayoutManager

    companion object {
        const val ROLE_MASYARAKAT = "masyarakat"
        const val ROLE_PETUGAS = "petugas"

        const val TANGGAPAN_REQUEST_CODE = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initComponents()
    }

    private fun initComponents() {
        btnLaporkan = find(R.id.main_laporan_button)
        btnLaporkanEmpty = find(R.id.main_laporan_button_2)
        btnLogout = find(R.id.main_logout_button)
        container = find(R.id.main_refresh_layout)
        rvLaporan = find(R.id.main_recycler_view)
        noDataContainer = find(R.id.main_no_data_container)
        filterButton = find(R.id.main_filter_button)
        petugasText = find(R.id.main_petugas_text)
        mainNoDataDesc = find(R.id.main_no_data_desc)
        accountName = find(R.id.main_account_name)
        nikNumber = find(R.id.main_account_nik)
        edtSearch = find(R.id.main_search_laporan)

        profile = getUserData(this)
        extraRole = profile.role_user.toString()

        btnLaporkan.setOnClickListener(this)
        btnLaporkanEmpty.setOnClickListener(this)
        btnLogout.setOnClickListener(this)
        filterButton.setOnClickListener(this)

        if (extraRole == ROLE_PETUGAS) {
            btnLaporkan.visibility = View.GONE
            petugasText.visibility = View.VISIBLE
            btnLaporkanEmpty.visibility = View.GONE
            mainNoDataDesc.text = getString(R.string.empty_desc_petugas)
        }

        setProfileData()

        laporanListAdapter = LaporanListAdapter(itemList) { complaint ->
            if (extraRole == ROLE_MASYARAKAT) {
                startActivity(intentFor<DetailLaporanActivity>(
                    DetailLaporanActivity.EXTRA_ID_LAPORAN to complaint.id,
                    DetailLaporanActivity.EXTRA_ROLE_USER to extraRole
                ))
            } else {
                startActivityForResult(intentFor<DetailLaporanActivity>(
                    DetailLaporanActivity.EXTRA_ID_LAPORAN to complaint.id,
                    DetailLaporanActivity.EXTRA_ROLE_USER to extraRole
                ), TANGGAPAN_REQUEST_CODE)
            }
        }
        linearLayoutManager = LinearLayoutManager(this)
        rvLaporan.layoutManager = linearLayoutManager
        rvLaporan.adapter = laporanListAdapter
        rvLaporan.isNestedScrollingEnabled = true

        mainViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MainViewModel::class.java)
        mainViewModel.getLaporan(getToken(this), extraRole, verified, page, searchQuery, DataManagerService())

        container.isRefreshing = true

        scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                mainViewModel.getLaporan(getToken(this@MainActivity), extraRole, verified, (page + 1), searchQuery, DataManagerService())
            }
        }

        rvLaporan.addOnScrollListener(scrollListener)

        container.setOnRefreshListener {
            reRequestDataLaporan()
        }

        edtSearch.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when(actionId) {
                EditorInfo.IME_ACTION_SEARCH -> {
                    searchQuery = edtSearch.text.trim().toString()
                    reRequestDataLaporan()
                    true
                }
                else -> false
            }
        }

        observeViewModel()
    }

    private fun setProfileData() {
        accountName.text = profile.name.toString()
        nikNumber.text = profile.nik.toString()
    }

    private fun observeViewModel() {
        mainViewModel.getLaporanData().observe(this, Observer {
            if(it.data != null) {
                if(it.success!!) {
                    it?.data.complaint?.data?.let { it1 -> itemList.addAll(it1) }
                    if(itemList.isNullOrEmpty()) {
                        showNoDataIllustration()
                    } else {
                        laporanListAdapter.notifyDataSetChanged()
                        noDataContainer.visibility = View.GONE
                        rvLaporan.visibility = View.VISIBLE
                    }
                    container.isRefreshing = false
                }
            }
        })

        mainViewModel.getLaporanErrorData().observe(this, Observer {
            if(it != null) {
                val errorMessage = Utils.getErrorList(it.data?.errors!!)
                container.isRefreshing = true
                container.snackbar(errorMessage).show()
            }
        })
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.main_laporan_button -> toTambahLaporan()
            R.id.main_laporan_button_2 -> toTambahLaporan()
            R.id.main_logout_button -> logoutValidation()
            R.id.main_filter_button -> filter()
        }
    }

    private fun reRequestDataLaporan() {
        itemList.clear()
        laporanListAdapter.notifyDataSetChanged()
        scrollListener.resetState()
        mainViewModel.getLaporan(getToken(this), extraRole, verified, page, searchQuery, DataManagerService())
    }

    private fun filter() {
        val popup = PopupMenu(this, filterButton)
        popup.menuInflater.inflate(R.menu.filter_menu, popup.menu)
        popup.setOnMenuItemClickListener {
            when(it.itemId) {
                R.id.filter_verified -> {
                    verified = true
                    reRequestDataLaporan()
                }
                R.id.filter_no_verified -> {
                    verified = false
                    reRequestDataLaporan()
                }
            }
            true
        }

        popup.show()
    }

    private fun showNoDataIllustration() {
        itemList.clear()
        laporanListAdapter.notifyDataSetChanged()
        scrollListener.resetState()
        rvLaporan.visibility = View.GONE
        noDataContainer.visibility = View.VISIBLE
    }

    private fun toTambahLaporan(): Unit = startActivity(intentFor<TambahLaporanActivity>())

    private fun toLoginPage(): Unit = startActivity(intentFor<AuthActivity>())

    private fun logoutValidation() {
        val logoutDialog: AlertDialog.Builder? = this.let { fragmentActivity ->
            Utils.createAlertDialog(
                fragmentActivity, R.string.keluar,
                R.string.validation_logout_desc, R.string.yakin, R.string.tidak, this
            ) { _: DialogInterface, _: Int ->
                logout()
            }
        }
        logoutDialog?.show()
    }

    @Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    private fun logout() {
        try {
            database.use {
                delete(ProfileModel.TABLE_PROFILE)
            }
            toLoginPage()
        } catch(e: SQLiteConstraintException) {
            container.snackbar(e.localizedMessage).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == TANGGAPAN_REQUEST_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                val id = data?.getIntExtra(DetailLaporanActivity.EXTRA_ID_LAPORAN, 0)
                val index = itemList.indexOfFirst { it.id == id }
                itemList.removeAt(index)
                laporanListAdapter.notifyItemRemoved(index)
            }
        }
    }

    override fun setNegativeButton(dialog: DialogInterface, which: Int) {
        dialog.dismiss()
    }

}
