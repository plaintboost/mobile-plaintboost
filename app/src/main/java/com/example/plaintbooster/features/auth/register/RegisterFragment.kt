package com.example.plaintbooster.features.auth.register

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.example.plaintbooster.R
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.viewmodel.RegisterViewModel
import com.example.plaintbooster.utils.Utils.createAlertDialog
import com.example.plaintbooster.utils.Utils.getErrorList
import com.example.plaintbooster.utils.Utils.hideLoading
import com.example.plaintbooster.utils.Utils.showLoading
import com.example.plaintbooster.utils.ValidationDialogCallback
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.support.v4.find

class RegisterFragment : Fragment(), ValidationDialogCallback {

    private lateinit var emailInput: EditText
    private lateinit var namaLengkapInput: EditText
    private lateinit var usernameInput: EditText
    private lateinit var nikInput: EditText
    private lateinit var teleponInput: EditText
    private lateinit var passwordInput: EditText
    private lateinit var confirmPasswordInput: EditText
    private lateinit var daftar: Button
    private lateinit var masuk: TextView
    private lateinit var progressBar: ProgressBar
    private lateinit var container: FrameLayout

    private lateinit var registerViewModel: RegisterViewModel

    private var showValidationDialog = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        registerViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(RegisterViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initComponents()
    }

    private fun initComponents() {
        emailInput = find(R.id.register_email_input)
        namaLengkapInput = find(R.id.register_nama_lengkap_input)
        usernameInput = find(R.id.register_username_input)
        nikInput = find(R.id.register_nik_input)
        teleponInput = find(R.id.register_telp_input)
        passwordInput = find(R.id.register_password_input)
        confirmPasswordInput = find(R.id.register_reinput_password_input)
        daftar = find(R.id.register_button_register)
        masuk = find(R.id.register_masuk_button)
        container = find(R.id.register_container)
        progressBar = find(R.id.register_progress_bar)

        registerViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(RegisterViewModel::class.java)

        daftar.setOnClickListener {
            fieldCheck()
            val validationDialog: AlertDialog.Builder? = activity?.let {
                createAlertDialog(it, R.string.validation_title, R.string.validation_register_desc,
                    R.string.yakin,null, this) { _: DialogInterface, _: Int ->
                    showLoading(progressBar)
                    registerViewModel.register(
                        name = namaLengkapInput.text.toString().trim(),
                        username = usernameInput.text.toString().trim(),
                        email = emailInput.text.toString().trim(),
                        password = passwordInput.text.toString().trim(),
                        nik = nikInput.text.toString().trim(),
                        telp = teleponInput.text.toString().trim(),
                        dataManagerService = DataManagerService()
                    )
                }
            }
            if(showValidationDialog) {
                validationDialog?.show()
            }
        }

        masuk.setOnClickListener {
            activity?.onBackPressed()
        }

        observeViewModel()
    }

    private fun fieldCheck() {
        when {
            emailInput.text.toString().trim().isEmpty() -> {
                emailInput.error = "Isi email"
                showValidationDialog = false
                return
            }
            namaLengkapInput.text.toString().trim().isEmpty() -> {
                namaLengkapInput.error = "Isi nama lengkap"
                showValidationDialog = false
                return
            }
            usernameInput.text.toString().trim().isEmpty() -> {
                usernameInput.error = "Isi username"
                showValidationDialog = false
                return
            }
            nikInput.text.toString().trim().isEmpty() -> {
                nikInput.error = "Isi nik"
                showValidationDialog = false
                return
            }
            teleponInput.text.toString().trim().isEmpty() -> {
                teleponInput.error = "Isi telepon"
                showValidationDialog = false
                return
            }
            passwordInput.text.toString().trim().isEmpty() -> {
                passwordInput.error = "Isi password"
                showValidationDialog = false
                return
            }
            confirmPasswordInput.text.toString().trim().isEmpty() -> {
                confirmPasswordInput.error = "Isi konfirmasi password"
                showValidationDialog = false
                return
            }
            passwordInput.text.toString().trim() != confirmPasswordInput.text.toString().trim() -> {
                confirmPasswordInput.error = "Konfirmasi password tidak sesuai dengan password"
                showValidationDialog = false
                return
            }
        }
        showValidationDialog = true
    }

    private fun observeViewModel() {
        registerViewModel.getLoginData().observe(viewLifecycleOwner, Observer {
            if(it != null) {
                if(it.success!!) {
                    hideLoading(progressBar)
                    val loginDialog: AlertDialog.Builder? = activity?.let { fragmentActivity ->
                        createAlertDialog(fragmentActivity, R.string.masuk,
                            R.string.validation_register_login_desc, R.string.ok, null, null) { dialogInterface: DialogInterface, _: Int ->
                            dialogInterface.dismiss()
                        }
                    }
                    loginDialog?.show()
                    activity?.onBackPressed()
                } else {
                    hideLoading(progressBar)
                    container.snackbar("Something problem").show()
                }
            }
        })

        registerViewModel.getErrorLoginData().observe(viewLifecycleOwner, Observer {
            val errorMessage = getErrorList(it.data?.errors!!)
            hideLoading(progressBar)
            container.snackbar(errorMessage).show()
        })
    }

    override fun setNegativeButton(dialog: DialogInterface, which: Int) {
        dialog.dismiss()
    }

}
