package com.example.plaintbooster.features.laporan.tambahlaporan

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.plaintbooster.R
import com.example.plaintbooster.adapter.ImageListAdapter
import com.example.plaintbooster.features.MainActivity
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.viewmodel.TambahLaporanViewModel
import com.example.plaintbooster.utils.Utils
import com.example.plaintbooster.utils.Utils.createAlertDialog
import com.example.plaintbooster.utils.Utils.getToken
import com.example.plaintbooster.utils.Utils.hideLoading
import com.example.plaintbooster.utils.Utils.showLoading
import com.example.plaintbooster.utils.ValidationDialogCallback
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

class TambahLaporanActivity : AppCompatActivity(), View.OnClickListener, ValidationDialogCallback {

    private lateinit var btnLaporkan: Button
    private lateinit var btnBack: ImageView
    private lateinit var judulLaporan: EditText
    private lateinit var isiLaporan: EditText
    private lateinit var gambarRecyclerView: RecyclerView
    private lateinit var btnTambahGambar: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var container: ScrollView

    private lateinit var imageListAdapter: ImageListAdapter
    private lateinit var tambahLaporanViewModel: TambahLaporanViewModel
    private var showValidationDialog = false

    private val imageList: MutableList<String> = mutableListOf()
    private val realPathUri: MutableList<Uri> = mutableListOf()
    private var imageExist: MutableLiveData<Boolean> = MutableLiveData()

    private var requestImage: MutableList<MultipartBody.Part> = mutableListOf()
    private var judul: RequestBody? = null
    private var laporan: RequestBody? = null

    companion object {
        const val CHOOSE_IMAGE_REQUEST_CODE = 100
        const val REQUEST_CODE_READ_STORAGE_PERMISSION = 111
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_laporan)
        initComponents()
    }

    @SuppressLint("InlinedApi")
    private fun initComponents() {
        btnLaporkan = find(R.id.tambah_laporan_submit_button)
        btnBack = find(R.id.tambah_laporan_back_button)
        judulLaporan = find(R.id.tambah_laporan_form_judul_laporan)
        isiLaporan = find(R.id.tambah_laporan_form_laporan)
        gambarRecyclerView = find(R.id.tambah_laporan_image_recyclerview)
        btnTambahGambar = find(R.id.tambah_laporan_tambah_gambar)
        progressBar = find(R.id.tambah_laporan_progress_bar)
        container = find(R.id.tambah_laporan_container)

        btnLaporkan.setOnClickListener(this)
        btnBack.setOnClickListener(this)
        imageExist.postValue(false)

        imageListAdapter = ImageListAdapter(imageList) {
            toast("anda klik gambar")
        }
        gambarRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        gambarRecyclerView.adapter = imageListAdapter
        gambarRecyclerView.isNestedScrollingEnabled = true

        tambahLaporanViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(TambahLaporanViewModel::class.java)

        observeLiveData()
    }

    private fun observeLiveData() {
        tambahLaporanViewModel.getTambahLaporanData().observe(this, Observer {
            if(it != null) {
                hideLoading(progressBar)
                if(it.success!!) {
                    startActivity(intentFor<MainActivity>())
                } else {
                    container.snackbar("Something problem").show()
                    clearAllData(true)
                }
            }
        })

        tambahLaporanViewModel.getTambahLaporanErrorData().observe(this, Observer {
            val errorMessage = Utils.getErrorList(it.data?.errors!!)
            hideLoading(progressBar)
            container.snackbar(errorMessage).show()
            clearAllData(true)
        })

        imageExist.observe(this, Observer {
            if(it) {
                btnTambahGambar.text = getString(R.string.hapus_gambar)
                btnTambahGambar.setOnClickListener {
                    clearAllData()
                    imageListAdapter.notifyDataSetChanged()
                    gambarRecyclerView.visibility = View.GONE
                    imageExist.postValue(false)
                }
            } else {
                btnTambahGambar.text = getString(R.string.tambah_gambar)
                btnTambahGambar.setOnClickListener {
                    requestPermission()
                }
            }
        })
    }

    private fun clearAllData(withText: Boolean = false) {
        imageList.clear()
        realPathUri.clear()
        requestImage.clear()
        judul = null
        laporan = null
        if(withText) {
            judulLaporan.setText("")
            isiLaporan.setText("")
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE
        ), REQUEST_CODE_READ_STORAGE_PERMISSION)
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(requestCode == REQUEST_CODE_READ_STORAGE_PERMISSION) {
            if((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                val chooseImage = Intent(Intent.ACTION_OPEN_DOCUMENT)
                chooseImage.addCategory(Intent.CATEGORY_OPENABLE)
                chooseImage.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                chooseImage.type = "image/*"
                startActivityForResult(chooseImage, CHOOSE_IMAGE_REQUEST_CODE)
            } else {
                finishAffinity()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onClick(v: View?) {
        when(v?.id) {
           R.id.tambah_laporan_submit_button -> {
               fieldCheck()
               if(showValidationDialog) {
                   showValidation()
               }
           }
            R.id.tambah_laporan_back_button -> onBackPressed()
        }
    }

    private fun showValidation() {
        val loginDialog: AlertDialog.Builder? = this.let { fragmentActivity ->
            createAlertDialog(
                fragmentActivity, R.string.validation_title,
                R.string.validation_pengaduan_desc, R.string.yakin, R.string.tidak, this
            ) { _: DialogInterface, _: Int ->
                showLoading(progressBar)
                if(imageList.isEmpty()) {
                    Log.d(TambahLaporanActivity::class.java.simpleName, "TANPA GAMBAR")
                    tambahLaporanViewModel.postTambahLaporan(getToken(this), judulLaporan.text.trim().toString(), isiLaporan.text.trim().toString(), DataManagerService())
                } else {
                    convertToMultipart()
                    Log.d(TambahLaporanActivity::class.java.simpleName, "DENGAN GAMBAR")
                    tambahLaporanViewModel.postTambahLaporan(getToken(this), judul!!, laporan!!, requestImage, DataManagerService())
                }
            }
        }
        loginDialog?.show()
    }

    private fun fieldCheck() {
        when {
            judulLaporan.text.toString().trim().isEmpty() -> {
                judulLaporan.error = "Isi judul laporan"
                showValidationDialog = false
                return
            }
            isiLaporan.text.toString().trim().isEmpty() -> {
                isiLaporan.error = "Isi laporan"
                showValidationDialog = false
                return
            }
        }
        showValidationDialog = true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == CHOOSE_IMAGE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            imageList.clear()
            if(data?.clipData != null) {
                for(i in 0 until data.clipData!!.itemCount) {
                    imageList.add(data.clipData!!.getItemAt(i).uri.toString())
                    realPathUri.add(data.clipData!!.getItemAt(i).uri)
                }
            } else {
                imageList.add(data?.data.toString())
            }

            gambarRecyclerView.visibility = View.VISIBLE
            imageListAdapter.notifyDataSetChanged()
            imageExist.postValue(true)
        }
    }

    private fun convertToMultipart() {
        judul = RequestBody.create(MediaType.parse("text/plain"), judulLaporan.text.trim().toString())
        laporan = RequestBody.create(MediaType.parse("text/plain"), isiLaporan.text.trim().toString())
        if(realPathUri.isNotEmpty()) {
            realPathUri.forEach {
                try {
                    val file = com.example.plaintbooster.utils.FileUtils.getFile(this, it)
                    val requestFile = RequestBody.create(MediaType.parse(contentResolver.getType(it)), file)
                    requestImage.add(MultipartBody.Part.createFormData("foto[]", file.name, requestFile))
                } catch(e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun setNegativeButton(dialog: DialogInterface, which: Int) {
        dialog.dismiss()
    }
}
