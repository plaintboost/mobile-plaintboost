package com.example.plaintbooster.features.laporan.tambahlaporan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment

import com.example.plaintbooster.R
import org.jetbrains.anko.support.v4.find

class ValidationDialog : DialogFragment(), View.OnClickListener {

    private lateinit var btnYakin: Button
    private lateinit var btnTidak: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_validation_dialog, container, false)
    }

    interface SubmitListener {
        fun onSubmitListener(result: Boolean)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnYakin = find(R.id.tambah_laporan_validation_yakin)
        btnTidak = find(R.id.tambah_laporan_validation_tidak)

        btnYakin.setOnClickListener(this)
        btnTidak.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if(v?.id == R.id.tambah_laporan_validation_tidak) {
            dismiss()
        } else {
            val listener: SubmitListener = activity as SubmitListener
            listener.onSubmitListener(true)
            dismiss()
        }
    }

}
