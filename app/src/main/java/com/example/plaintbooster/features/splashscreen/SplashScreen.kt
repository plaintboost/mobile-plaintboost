package com.example.plaintbooster.features.splashscreen

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.example.plaintbooster.R
import com.example.plaintbooster.databases.ProfileModel
import com.example.plaintbooster.databases.database
import com.example.plaintbooster.features.MainActivity
import com.example.plaintbooster.features.auth.AuthActivity
import com.example.plaintbooster.features.onboarding.OnboardingPage
import com.example.plaintbooster.utils.PreferenceSetting
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import java.lang.Exception

class SplashScreen : AppCompatActivity() {

    private lateinit var preferenceSetting: PreferenceSetting
    private var firstTime: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        preferenceSetting = PreferenceSetting(applicationContext)
        firstTime = preferenceSetting.getPreferenceFirstTimeInstall()
        Handler().postDelayed(Runnable {
            val intentTo = if(firstTime) {
                if(checkTokenExists()) {
                    Intent(this@SplashScreen, MainActivity::class.java)
                } else {
                    Intent(this@SplashScreen, AuthActivity::class.java)
                }
            } else {
                Intent(this@SplashScreen, OnboardingPage::class.java)
            }
            startActivity(intentTo)
            finish()
        }, 1500)

    }

    private fun checkTokenExists(): Boolean {
        var resultCheck = false
        try {
            database.use {
                val result = select(ProfileModel.TABLE_PROFILE)
                val parser = result.parseList(classParser<ProfileModel>())
                resultCheck = !(parser.size > 1 || parser.isEmpty())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return resultCheck
    }
}
