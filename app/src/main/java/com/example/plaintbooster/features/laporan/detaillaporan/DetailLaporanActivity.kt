package com.example.plaintbooster.features.laporan.detaillaporan

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.plaintbooster.R
import com.example.plaintbooster.adapter.ImageListAdapter
import com.example.plaintbooster.features.MainActivity
import com.example.plaintbooster.models.Complaint
import com.example.plaintbooster.models.Foto
import com.example.plaintbooster.models.Responses
import com.example.plaintbooster.network.DataManagerService
import com.example.plaintbooster.network.viewmodel.DetailLaporanViewModel
import com.example.plaintbooster.network.viewmodel.TanggapanViewModel
import com.example.plaintbooster.utils.Utils
import com.example.plaintbooster.utils.Utils.DEFAULT_FORMAT
import com.example.plaintbooster.utils.Utils.getErrorList
import com.example.plaintbooster.utils.Utils.getFormatTime
import com.example.plaintbooster.utils.Utils.getToken
import com.example.plaintbooster.utils.Utils.hideLoading
import com.example.plaintbooster.utils.Utils.showLoading
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import org.w3c.dom.Text

class DetailLaporanActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var container: SwipeRefreshLayout
    private lateinit var btnBack: ImageView
    private lateinit var laporanTitle: TextView
    private lateinit var laporanImageRecyclerView: RecyclerView
    private lateinit var laporanPelapor: TextView
    private lateinit var laporanTanggal: TextView
    private lateinit var laporanDetailLaporan: TextView
    private lateinit var edtTanggapan: EditText
    private lateinit var btnTanggapi: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var detailLaporanContainer: LinearLayout
    private lateinit var tanggapanTitle: TextView
    private lateinit var containerResponsePetugas: LinearLayout
    private lateinit var containerResponseWaktu: LinearLayout
    private lateinit var petugas: TextView
    private lateinit var waktuDitanggapi: TextView
    private lateinit var tanggapanDesc: TextView

    private var imageList: MutableList<String> = mutableListOf()
    private var extraId = 0
    private var extraRoleUser = ""

    private lateinit var detailLaporanViewModel: DetailLaporanViewModel
    private lateinit var postTanggapanViewModel: TanggapanViewModel
    private lateinit var imageListAdapter: ImageListAdapter

    companion object {
        const val EXTRA_ID_LAPORAN = "extra_id_laporan"
        const val EXTRA_ROLE_USER = "extra_role_user"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_laporan)
        initComponents()
        setViewModel()
        observeViewModel()
        setEvent()
    }

    private fun initComponents() {
        container = find(R.id.detail_laporan_swipe_refresh)
        btnBack = find(R.id.detail_laporan_back_button)
        laporanTitle = find(R.id.detail_laporan_title)
        laporanImageRecyclerView = find(R.id.detail_laporan_image_recyclerview)
        laporanPelapor = find(R.id.detail_laporan_pelapor)
        laporanTanggal = find(R.id.detail_laporan_waktu_lapor)
        laporanDetailLaporan = find(R.id.detail_laporan_laporan)
        edtTanggapan = find(R.id.detail_laporan_tanggapan)
        btnTanggapi = find(R.id.detail_laporan_submit_button)
        progressBar = find(R.id.detail_laporan_progress_bar)
        detailLaporanContainer = find(R.id.detail_laporan_form_container)
        tanggapanTitle = find(R.id.detail_laporan_tanggapan_title)
        containerResponsePetugas = find(R.id.detail_laporan_petugas_container)
        containerResponseWaktu = find(R.id.detail_laporan_waktu_ditanggapi_container)
        petugas = find(R.id.detail_laporan_petugas)
        waktuDitanggapi = find(R.id.detail_laporan_waktu_ditanggapi)
        tanggapanDesc = find(R.id.detail_laporan_tanggapan_desc)

        btnBack.setOnClickListener(this)
        btnTanggapi.setOnClickListener(this)

        imageListAdapter = ImageListAdapter(imageList, true) { /* isikan event apabila gambar di klik */ }
        laporanImageRecyclerView.adapter = imageListAdapter
        laporanImageRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        laporanImageRecyclerView.isNestedScrollingEnabled = true

        extraId = intent.getIntExtra(EXTRA_ID_LAPORAN, 0)
        extraRoleUser = intent.getStringExtra(EXTRA_ROLE_USER)?:MainActivity.ROLE_PETUGAS
    }

    private fun hiddenRecyclerview() {
        laporanImageRecyclerView.visibility = View.GONE
    }

    private fun setEvent() {
        container.setOnRefreshListener {
            detailLaporanViewModel.getDetailLaporan(getToken(this), extraId, DataManagerService())
            container.isRefreshing = false
        }
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.detail_laporan_back_button -> onBackPressed()
            R.id.detail_laporan_submit_button -> {
                if(edtTanggapan.text.trim().toString().isEmpty()) {
                    edtTanggapan.error = "Isi tanggapan"
                    return
                } else {
                    postTanggapanViewModel.postTanggapan(getToken(this), extraId, edtTanggapan.text.trim().toString(), DataManagerService())
                    showLoading(progressBar)
                }
            }
        }
    }

    private fun setViewModel() {
        detailLaporanViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(DetailLaporanViewModel::class.java)
        detailLaporanViewModel.getDetailLaporan(getToken(this), extraId, DataManagerService())
        postTanggapanViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(TanggapanViewModel::class.java)
    }

    private fun observeViewModel() {
        detailLaporanViewModel.getLaporanData().observe(this, Observer {
            if(it?.data != null) {
                val data = it.data
                if(data.complaint?.foto!!.isNotEmpty()) {
                    data.complaint.foto.forEach { foto ->
                        imageList.add(foto.foto.toString())
                    }
                    imageListAdapter.notifyDataSetChanged()
                } else {
                    hiddenRecyclerview()
                }
                setData(data.complaint, data.response)
                hideLoading(progressBar)
            }
        })

        detailLaporanViewModel.getLaporanErrorData().observe(this, Observer {
            if(it != null) {
                val errorMessage = getErrorList(it.data?.errors!!)
                hideLoading(progressBar)
                container.snackbar(errorMessage).show()
            }
        })

        postTanggapanViewModel.getTanggapanData().observe(this, Observer {
            if(it?.success!!) {
                val resultIntent = Intent()
                resultIntent.putExtra(EXTRA_ID_LAPORAN, extraId)
                setResult(Activity.RESULT_OK, resultIntent)
                finish()
            }
        })

        postTanggapanViewModel.getTanggapanErrorData().observe(this, Observer {
            if(it != null) {
                val errorMessage = getErrorList(it.data?.errors!!)
                hideLoading(progressBar)
                container.snackbar(errorMessage).show()
            }
        })
    }

    private fun setData(complaint: Complaint, response: Responses?) {
        laporanTitle.text = complaint.judul_laporan.toString()
        laporanPelapor.text = complaint.user?.name.toString()
        laporanTanggal.text = getFormatTime(DEFAULT_FORMAT, complaint.created_at.toString())
        laporanDetailLaporan.text = complaint.laporan.toString()
        hideElement((extraRoleUser == MainActivity.ROLE_MASYARAKAT) || complaint.verified == 1)
        if(extraRoleUser == MainActivity.ROLE_MASYARAKAT) {
            if(response == null) {
                tanggapanDesc.text = "Belum ditanggapi"
                containerResponsePetugas.visibility = View.GONE
                containerResponseWaktu.visibility = View.GONE
            } else {
                tanggapanDesc.text = response.tanggapan.toString()
                petugas.text = response.petugas?.name.toString()
                waktuDitanggapi.text = getFormatTime(DEFAULT_FORMAT, response.created_at.toString())
            }
        } else {
            if(response != null) {
                tanggapanDesc.text = response.tanggapan.toString()
                petugas.text = response.petugas?.name.toString()
                waktuDitanggapi.text = getFormatTime(DEFAULT_FORMAT, response.created_at.toString())
            }
        }
    }

    private fun allInvisible() {
        detailLaporanContainer.visibility = View.INVISIBLE
        btnTanggapi.visibility = View.INVISIBLE
    }

    private fun allVisible() {
        detailLaporanContainer.visibility = View.VISIBLE
        btnTanggapi.visibility = View.VISIBLE
    }

    private fun hideElement(masyarakat: Boolean) {
        if(masyarakat) {
            tanggapanTitle.visibility = View.GONE
            edtTanggapan.visibility = View.GONE
            btnTanggapi.visibility = View.GONE
            tanggapanDesc.visibility = View.VISIBLE
            containerResponsePetugas.visibility = View.VISIBLE
            containerResponseWaktu.visibility = View.VISIBLE
        } else {
            tanggapanTitle.visibility = View.VISIBLE
            edtTanggapan.visibility = View.VISIBLE
            btnTanggapi.visibility = View.VISIBLE
            tanggapanDesc.visibility = View.GONE
            containerResponsePetugas.visibility = View.GONE
            containerResponseWaktu.visibility = View.GONE
        }
    }

}
