package com.example.plaintbooster.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.plaintbooster.BuildConfig
import com.example.plaintbooster.R
import org.jetbrains.anko.find

class ImageListAdapter(
    private val items: List<String>,
    private val fromUrl: Boolean = false,
    private val listener: (image: ImageView) -> Unit
): RecyclerView.Adapter<ImageListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_image_slider, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind(items[position], fromUrl, listener)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private var image: ImageView = view.find(R.id.item_image)
        private var context: Context = view.context
        private var BASE_HOST = BuildConfig.BASE_URL

        fun onBind(item: String, fromUrl: Boolean, listener: (image: ImageView) -> Unit) {
            var loadItem = ""
            if(fromUrl) {
                loadItem = "$BASE_HOST${item}"
            } else {
                loadItem = item
            }
            Glide.with(context).load(loadItem).into(image)
            image.setOnClickListener {
                listener(image)
            }
        }

    }

}