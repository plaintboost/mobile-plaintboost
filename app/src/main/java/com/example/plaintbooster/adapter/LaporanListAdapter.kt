package com.example.plaintbooster.adapter

import android.content.Context
import android.text.Layout
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.plaintbooster.BuildConfig
import com.example.plaintbooster.R
import com.example.plaintbooster.models.Complaint
import com.example.plaintbooster.models.laporan.GetLaporan
import com.example.plaintbooster.utils.Utils
import com.example.plaintbooster.utils.Utils.DEFAULT_FORMAT
import com.example.plaintbooster.utils.Utils.getFormatTime
import org.jetbrains.anko.find
import org.w3c.dom.Text

class LaporanListAdapter(
    private val items: List<Complaint>,
    private val listener: (Complaint) -> Unit
): RecyclerView.Adapter<LaporanListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_card_laporan, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(items[position], listener)
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val image: ImageView = view.find(R.id.item_laporan_image)
        private val title: TextView = view.find(R.id.item_laporan_title)
        private val author: TextView = view.find(R.id.item_laporan_name)
        private val date: TextView = view.find(R.id.item_laporan_tanggal)
        private val tick: ImageView = view.find(R.id.item_laporan_tick)
        private val container: LinearLayout = view.find(R.id.item_laporan_container)

        fun onBind(item: Complaint, listener: (Complaint) -> Unit) {
            title.text = item.judul_laporan.toString()
            author.text = item.user?.name.toString()
            date.text = getFormatTime(DEFAULT_FORMAT, item.created_at!!)
            if(item.verified == 0) {
                tick.visibility = View.GONE
            }
            container.setOnClickListener {
                listener(item)
            }
        }

    }
}