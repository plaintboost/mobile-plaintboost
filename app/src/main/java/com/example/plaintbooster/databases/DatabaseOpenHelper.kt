package com.example.plaintbooster.databases

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class DatabaseOpenHelper(context: Context): ManagedSQLiteOpenHelper(context, "db_plaintbooster.db", null, 1) {

    companion object {
        private var INSTANCE: DatabaseOpenHelper? = null

        @Synchronized
        fun getInstance(context: Context): DatabaseOpenHelper {
            if(INSTANCE == null) {
                INSTANCE = DatabaseOpenHelper(context)
            }
            return INSTANCE as DatabaseOpenHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(ProfileModel.TABLE_PROFILE, true,
            ProfileModel.PROFILE_ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            ProfileModel.PROFILE_NAME to TEXT,
            ProfileModel.PROFILE_USERNAME to TEXT,
            ProfileModel.PROFILE_EMAIL to TEXT,
            ProfileModel.PROFILE_NIK to TEXT,
            ProfileModel.PROFILE_TELP to TEXT,
            ProfileModel.PROFILE_ROLE to TEXT,
            ProfileModel.PROFILE_TOKEN to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(ProfileModel.TABLE_PROFILE, true)
    }

}

val Context.database: DatabaseOpenHelper
    get() = DatabaseOpenHelper.getInstance(applicationContext)