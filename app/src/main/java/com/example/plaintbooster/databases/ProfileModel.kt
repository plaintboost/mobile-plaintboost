package com.example.plaintbooster.databases

data class ProfileModel(
    var id: Int? = null,
    var name: String? = null,
    var username: String? = null,
    var email: String? = null,
    var nik: String? = null,
    var telp: String? = null,
    val role_user: String? = null,
    var token: String? = null
) {
    companion object {
        const val TABLE_PROFILE = "PROFILE_TABLE"
        const val PROFILE_ID = "PROFILE_ID"
        const val PROFILE_NAME = "PROFILE_NAME"
        const val PROFILE_USERNAME = "PROFILE_USERNAME"
        const val PROFILE_EMAIL = "PROFILE_EMAIL"
        const val PROFILE_NIK = "PROFILE_NIK"
        const val PROFILE_TELP = "PROFILE_TELP"
        const val PROFILE_ROLE = "PROFILE_ROLE"
        const val PROFILE_TOKEN = "PROFILE_TOKEN"
    }
}